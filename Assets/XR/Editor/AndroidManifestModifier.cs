﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Android;
using System.IO;
using System.Text;
using System.Xml;

public class AndroidManifestModifier : IPostGenerateGradleAndroidProject
{
    public int callbackOrder
    {
        get
        {
            return 0;
        }
    }

    private const string _url = "http://schemas.android.com/apk/res/android";

    public void OnPostGenerateGradleAndroidProject(string path)
    {
        string manifestPath = path + "/src/main/AndroidManifest.xml";
        XmlDocument manifest = new XmlDocument();

        try
        {
            manifest.Load(manifestPath);
            //AddExportedFalse(manifest);
            AddMetaDataXRSpace(manifest);
            AddSDKVersion(manifest);
            AddSDKVersionCode(manifest);
            manifest.Save(manifestPath);
        }
        catch
        {
            Debug.Log("Modify manifest failed");
        }
    }

    private void AddExportedFalse(XmlDocument document)
    {
        XmlNamespaceManager nsMgr = new XmlNamespaceManager(document.NameTable);
        nsMgr.AddNamespace("android", _url);

        XmlNode activityNode = document.SelectSingleNode("/manifest/application/activity", nsMgr);
        XmlAttribute attr = (XmlAttribute)activityNode.Attributes.GetNamedItem("android:exported");
        if(attr != null)
        {
            attr.Value = "false";
        }
        else
        {
            attr = document.CreateAttribute("android:exported", _url);
            attr.Value = "false";
            activityNode.Attributes.Append(attr);
        }
    }

    private void AddMetaDataXRSpace(XmlDocument document)
    {
        XmlNamespaceManager nsMgr = new XmlNamespaceManager(document.NameTable);
        nsMgr.AddNamespace("android", _url);

        XmlNode applicationNode = document.SelectSingleNode("/manifest/application", nsMgr);
        XmlNode meta = document.CreateNode(XmlNodeType.Element, "meta-data", applicationNode.NamespaceURI);
        XmlAttribute attrName = document.CreateAttribute("android:name", _url);
        XmlAttribute attrValue = document.CreateAttribute("android:value", _url);
        attrName.Value = "com.XRSpace.VR";
        attrValue.Value = "true";
        meta.Attributes.Append(attrName);
        meta.Attributes.Append(attrValue);
        applicationNode.AppendChild(meta);
    }

    private void AddSDKVersion(XmlDocument document)
    {
        XmlNamespaceManager nsMgr = new XmlNamespaceManager(document.NameTable);
        nsMgr.AddNamespace("android", _url);

        XmlNode applicationNode = document.SelectSingleNode("/manifest/application", nsMgr);
        XmlNode meta = document.CreateNode(XmlNodeType.Element, "meta-data", applicationNode.NamespaceURI);
        XmlAttribute attrName = document.CreateAttribute("android:name", _url);
        XmlAttribute attrValue = document.CreateAttribute("android:value", _url);
        attrName.Value = "SDK-Version";
        attrValue.Value = XRSpace.Platform.InputDevice.XRInputManager.VERSION;
        meta.Attributes.Append(attrName);
        meta.Attributes.Append(attrValue);
        applicationNode.AppendChild(meta);
    }

    private void AddSDKVersionCode(XmlDocument document)
    {
        XmlNamespaceManager nsMgr = new XmlNamespaceManager(document.NameTable);
        nsMgr.AddNamespace("android", _url);

        XmlNode applicationNode = document.SelectSingleNode("/manifest/application", nsMgr);
        XmlNode meta = document.CreateNode(XmlNodeType.Element, "meta-data", applicationNode.NamespaceURI);
        XmlAttribute attrName = document.CreateAttribute("android:name", _url);
        XmlAttribute attrValue = document.CreateAttribute("android:value", _url);
        attrName.Value = "SDK-VersionCode";
        attrValue.Value = ParseVersionCode().ToString();
        meta.Attributes.Append(attrName);
        meta.Attributes.Append(attrValue);
        applicationNode.AppendChild(meta);
    }

    private int ParseVersionCode()
    {
        string[] versionStrings = XRSpace.Platform.InputDevice.XRInputManager.VERSION.Split(new char[1] { '.' });
        if (versionStrings.Length >= 3)
        {
            return int.Parse(versionStrings[0]) * 1000000 + int.Parse(versionStrings[1]) * 10000 + int.Parse(versionStrings[2]);
        }

        return 0;
    }
}
