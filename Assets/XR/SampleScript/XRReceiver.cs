﻿using UnityEngine;
using XRSpace.Platform.InputDevice;

public class XRReceiver : MonoBehaviour
{
    private Vector2 mouseNDC = Vector2.zero;
    private Vector2 mouseRotPos = Vector2.zero;
    private Vector2 mouseMovPos = Vector2.zero;

    private void Update()
    {
        if (XRInputManager.Instance.EditorSimulation)
        {
            if (Application.isEditor)
            {
                if (Input.GetMouseButton(1))    // 1/Right mouse button
                {
                    mouseRotPos.x += Input.GetAxis("Mouse X") * 5f;
                    mouseRotPos.y += Input.GetAxis("Mouse Y") * 5f;

                    transform.rotation = Quaternion.Euler(-mouseRotPos.y, mouseRotPos.x, 0);
                }

                if (Input.GetMouseButton(2))    // 2/Middle mouse button
                {
                    mouseMovPos.x = Input.GetAxis("Mouse X");
                    mouseMovPos.y = Input.GetAxis("Mouse Y");
                    mouseMovPos.x += Input.GetAxis("Mouse X") * 0.25f; //0.25=speed
                    mouseMovPos.y += Input.GetAxis("Mouse Y") * 0.25f;

                    transform.position += -transform.right * Input.GetAxis("Mouse X") * 0.1f;
                    transform.position += -transform.up * Input.GetAxis("Mouse Y") * 0.1f;
                }

                if (Input.GetAxis("Mouse ScrollWheel") != 0)
                {
                    transform.position += transform.forward * Input.GetAxis("Mouse ScrollWheel");
                }
            }
        }
    }
}