﻿using UnityEngine;
using XRSpace.Platform.InputDevice;
using XRSpace.Platform.VRcore;

public class XRHandlerReceiver : MonoBehaviour
{
    public Vector3 Handler3DofPosition = Vector3.zero;
    public XRHandlerDeviceType Device;
    public HandlerMode HandlerMode = HandlerMode.Auto;
    public bool EnableHandRotation = true;
    [SerializeField]
    private bool _isHandlerChildOfXRManager = false;
    private XRHandlerData _currentData;
    private XRHandlerType _currentHandlerType = XRHandlerType.None;
    private Quaternion _recenterRotation = Quaternion.identity;
    private Quaternion _defaultRotateR = new Quaternion(-0.41f, 0.41f, 0.58f, 0.58f);
    private Quaternion _defaultRotateL = new Quaternion(-0.41f, -0.41f, -0.58f, 0.58f);

    private void OnEnable()
    {
        XREvent.OnRecenterTrackingBefore += UpdateRecenterRotation;
        ControllerBehavior.OnTeleportEnd += ModifyRecenterByTP;
    }

    private void OnDisable()
    {
        XREvent.OnRecenterTrackingBefore -= UpdateRecenterRotation;
        ControllerBehavior.OnTeleportEnd -= ModifyRecenterByTP;
    }

    // Update is called once per frame
    void Update()
    {
        _currentData = XRInputManager.Instance.GetInputData<XRHandlerData>((XRDeviceType)Device);
        if (_currentData == null)
            return;
        ProcessHandlerType();
        if (_isHandlerChildOfXRManager)
            ProcessTransformChildOfXRManager();
        else
            ProcessTransform();

        //recenter
        if (XRInputManager.Instance.ButtonDown((XRDeviceType)Device, XRControllerButton.Action))
            Recenter();
    }

    private void ProcessHandlerType()
    {
        switch (HandlerMode)
        {
            case HandlerMode.Auto:
                _currentHandlerType = _currentData.HandlerType;
                break;
            case HandlerMode.Controller_3Dof:
                if (_currentData.HandlerType == XRHandlerType.Controller3Dof || _currentData.HandlerType == XRHandlerType.Controller6Dof)
                    _currentHandlerType = XRHandlerType.Controller3Dof;
                else
                    _currentHandlerType = XRHandlerType.None;
                break;
            case HandlerMode.Controller_6Dof:
                if (_currentData.HandlerType == XRHandlerType.Controller6Dof)
                    _currentHandlerType = XRHandlerType.Controller6Dof;
                else
                    _currentHandlerType = XRHandlerType.None;
                break;
            case HandlerMode.Hand:
                if (_currentData.HandlerType == XRHandlerType.Hand || _currentData.HandlerType == XRHandlerType.Controller6Dof || Application.isEditor)
                    _currentHandlerType = XRHandlerType.Hand;
                else
                    _currentHandlerType = XRHandlerType.None;
                break;
        }
    }

    private void ProcessTransformChildOfXRManager()
    {
        //Debug.LogWarning("kiter _currentHandlerType: " + _currentHandlerType);
        if (_currentHandlerType == XRHandlerType.Controller3Dof ||
            _currentHandlerType == XRHandlerType.None ||
            _currentData.IsHandLostTracking)
            transform.localPosition = XRManager.Instance.transform.InverseTransformDirection(XRManager.Instance.head.TransformDirection(Handler3DofPosition)) + XRManager.Instance.head.localPosition;
        else
            transform.localPosition = XRManager.Instance.transform.InverseTransformDirection(XRManager.Instance.head.TransformDirection(_currentData.Position)) + XRManager.Instance.head.localPosition;

        if (_currentHandlerType == XRHandlerType.Hand)
        {
            if (EnableHandRotation)
                transform.localRotation = XRManager.Instance.head.localRotation * _currentData.Rotation;
            else
            {
                if (Device == XRHandlerDeviceType.HANDLER_LEFT)
                    transform.localRotation = XRManager.Instance.head.localRotation * _defaultRotateL;
                if (Device == XRHandlerDeviceType.HANDLER_RIGHT)
                    transform.localRotation = XRManager.Instance.head.localRotation * _defaultRotateR;
            }
        }
        else if (!Application.isEditor || !XRInputManager.Instance.EditorSimulation)
            transform.localRotation = _recenterRotation * _currentData.Rotation;
    }

    private void ProcessTransform()
    {
        //Debug.LogWarning("kiter _currentHandlerType: " + _currentHandlerType);
        if (_currentHandlerType == XRHandlerType.Controller3Dof ||
            _currentHandlerType == XRHandlerType.None ||
            _currentData.IsHandLostTracking)
            transform.localPosition = XRManager.Instance.head.TransformDirection(Handler3DofPosition) + XRManager.Instance.head.position;
        else
            transform.localPosition = _currentData.GlobalPosition;

        if (_currentHandlerType == XRHandlerType.Hand)
        {
            if(EnableHandRotation)
                transform.localRotation = _currentData.GlobalRotation;
            else
            {
                if (Device == XRHandlerDeviceType.HANDLER_LEFT)
                    transform.localRotation = XRManager.Instance.head.rotation * _defaultRotateL;
                if (Device == XRHandlerDeviceType.HANDLER_RIGHT)
                    transform.localRotation = XRManager.Instance.head.rotation * _defaultRotateR;
            }
        }
        else if (!Application.isEditor || !XRInputManager.Instance.EditorSimulation)
            transform.localRotation = _recenterRotation * _currentData.Rotation;
    }

    private void Recenter()
    {
        XRInputManager.Instance.Recenter((XRDeviceType)Device);
        if (_isHandlerChildOfXRManager)
            _recenterRotation = Quaternion.Euler(0, XRManager.Instance.head.localRotation.eulerAngles.y, 0);
        else
            _recenterRotation = Quaternion.Euler(0, XRManager.Instance.head.rotation.eulerAngles.y, 0);
    }

    private void UpdateRecenterRotation()
    {
        var from = Vector3.ProjectOnPlane(XRManager.Instance.head.forward, Vector3.up);
        var to = Vector3.ProjectOnPlane(XRManager.Instance.transform.forward, Vector3.up);
        _recenterRotation *= Quaternion.FromToRotation(from, to);
    }

    private void ModifyRecenterByTP(Quaternion modifyQuaternion)
    {
        if (!_isHandlerChildOfXRManager)
            _recenterRotation *= modifyQuaternion;
    }
}
