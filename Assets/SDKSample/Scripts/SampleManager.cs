﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XRSpace.Platform.IME;
using XRSpace.Platform.InputDevice;
using XRSpace.Platform.VRcore;

public class SampleManager : MonoBehaviour
{
    public GameObject XRHandlerL = null;
    public GameObject XRHandlerR = null;
    private Vector3 _depthScaleFactor = new Vector3(1.0f, 1.0f, 1.0f);
    [SerializeField]
    private Text FPS = null;

    // Use this for initialization
    void Start()
    {
        Input.backButtonLeavesApp = true;
        XRInputManager.Instance.SetHandPositionScale(_depthScaleFactor);

		XRActionGestureManager.ActionDetectedEvent += (type, action) =>
		{
			if (type == XRDeviceType.HANDLER_LEFT && action == XRActionGesture.Grab_Inward)
			{
				XRIMEEventSystem.ActiveIme(false);
			}
		};
	}

    private void OnEnable()
    {
		//XRIMEManager.OnIMEBegin += EnableHandRotation;
		//XRIMEManager.OnIMEEnd += DisableHandRotation;
		XRIMEEventSystem.OnBeginIME += EnableHandRotation;
		XRIMEEventSystem.OnEndIME += DisableHandRotation;
	}

    private void OnDisable()
    {
		//XRIMEManager.OnIMEBegin -= EnableHandRotation;
		//XRIMEManager.OnIMEEnd -= DisableHandRotation;
		XRIMEEventSystem.OnBeginIME -= EnableHandRotation;
		XRIMEEventSystem.OnEndIME -= DisableHandRotation;
	}

    public void SetGestureSimulation(bool isOn)
    {
        XRHandlerR.GetComponentInChildren<XRHandlerSwitch>().HandlerMode = isOn ? HandlerMode.Hand : HandlerMode.Auto;
        XRHandlerL.GetComponentInChildren<XRHandlerSwitch>().HandlerMode = isOn ? HandlerMode.Hand : HandlerMode.Auto;
    }

    private void EnableHandRotation()
    {
        XRHandlerR.GetComponentInChildren<XRHandlerReceiver>().EnableHandRotation = false;
        XRHandlerL.GetComponentInChildren<XRHandlerReceiver>().EnableHandRotation = false;
    }

    private void DisableHandRotation()
	{
		XRHandlerR.GetComponentInChildren<XRHandlerReceiver>().EnableHandRotation = true;
        XRHandlerL.GetComponentInChildren<XRHandlerReceiver>().EnableHandRotation = true;
    }

	void Update()
	{
		ProcessHandlerActive();

		FPS.text = "FPS: " + XRManager.Instance.FPS.ToString();

		if (Input.GetKeyDown(KeyCode.K))
		{
			XRIMEEventSystem.ActiveIme(true);
		}
		if (Input.GetKeyDown(KeyCode.L))
		{
			XRIMEEventSystem.ActiveIme(false);
		}
	}

    private void ProcessHandlerActive()
    {
        //In editor do not change controller state 
        if (Application.isEditor)
        {
            if (XRHandlerL.activeInHierarchy)
                XRHandlerL.SetActive(false);
            if (!XRHandlerR.activeInHierarchy)
                XRHandlerR.SetActive(true);
            return;
        }

        //check have 3dof or 6dof controller connect and set active(true) to what controller connnected. 
        if (XRInputManager.Instance.IsConnect(XRDeviceType.HANDLER_RIGHT))
        {
            if (!XRHandlerR.activeInHierarchy)
                XRHandlerR.SetActive(true);
        }
        else
        {
            if (XRHandlerR.activeInHierarchy)
                XRHandlerR.SetActive(false);
        }
        if (XRInputManager.Instance.IsConnect(XRDeviceType.HANDLER_LEFT))
        {
            if (!XRHandlerL.activeInHierarchy)
                XRHandlerL.SetActive(true);
        }
        else
        {
            if (XRHandlerL.activeInHierarchy)
                XRHandlerL.SetActive(false);
        }
    }

    public void ResetScene()
    {
        var Go = FindObjectsOfType<CubeSelectionHint>();
        foreach (var obj in Go )
        {
            obj.ResetTrans();
        }
    }
}
